<?php
App::uses('AppModel', 'Model');
/**
 * Comment Model
 *
 * @property Foreign $Foreign
 */
class Comment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Foreign' => array(
			'className' => 'Foreign',
			'foreignKey' => 'foreign_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
