/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : nerdviral

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2013-10-16 23:23:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertisments
-- ----------------------------
DROP TABLE IF EXISTS `advertisments`;
CREATE TABLE `advertisments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture1` varchar(200) DEFAULT NULL,
  `picture2` varchar(200) DEFAULT NULL,
  `picture3` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of advertisments
-- ----------------------------

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_model` varchar(100) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `author_ip` varchar(20) DEFAULT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_email` varchar(100) NOT NULL,
  `author_website` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_comments_foreign_data` (`foreign_id`,`foreign_model`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for gossips
-- ----------------------------
DROP TABLE IF EXISTS `gossips`;
CREATE TABLE `gossips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `picture` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gossips
-- ----------------------------

-- ----------------------------
-- Table structure for headlines
-- ----------------------------
DROP TABLE IF EXISTS `headlines`;
CREATE TABLE `headlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `extension` varchar(200) DEFAULT NULL,
  `mimeType` varchar(200) DEFAULT NULL,
  `fileSize` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of headlines
-- ----------------------------

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_model` varchar(100) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `author_ip` varchar(20) DEFAULT NULL,
  `rating` float NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_ratings_foreign_data` (`foreign_id`,`foreign_model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratings
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', '2013-02-14 12:30:58', '2013-02-14 12:30:58');
INSERT INTO `roles` VALUES ('2', 'user', '2013-02-14 12:34:33', '2013-02-14 12:34:33');

-- ----------------------------
-- Table structure for tables
-- ----------------------------
DROP TABLE IF EXISTS `tables`;
CREATE TABLE `tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `played` int(11) DEFAULT NULL,
  `won` int(11) DEFAULT NULL,
  `draw` int(11) DEFAULT NULL,
  `lost` int(11) DEFAULT NULL,
  `goalFor` int(11) DEFAULT NULL,
  `goalAgainst` int(11) DEFAULT NULL,
  `goalDifferent` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tables
-- ----------------------------
INSERT INTO `tables` VALUES ('1', '1', '11', '22', '12', '7', '3', '32', '15', '17', '43', '8', '2013-10-16 13:25:17', '2013-10-16 13:25:17');
INSERT INTO `tables` VALUES ('2', '2', '10', '22', '10', '10', '2', '31', '17', '14', '40', '8', '2013-10-16 13:26:50', '2013-10-16 13:26:50');
INSERT INTO `tables` VALUES ('3', '3', '2', '22', '11', '7', '4', '32', '26', '6', '40', '8', '2013-10-16 13:27:37', '2013-10-16 13:27:37');
INSERT INTO `tables` VALUES ('4', '4', '4', '22', '10', '6', '6', '32', '20', '12', '36', '8', '2013-10-16 13:31:35', '2013-10-16 13:31:35');
INSERT INTO `tables` VALUES ('5', '5', '8', '22', '10', '5', '7', '36', '32', '4', '35', '8', '2013-10-16 13:32:23', '2013-10-16 13:32:23');
INSERT INTO `tables` VALUES ('6', '6', '1', '22', '10', '4', '8', '35', '25', '10', '34', '8', '2013-10-16 13:33:23', '2013-10-16 13:33:23');
INSERT INTO `tables` VALUES ('7', '7', '9', '22', '8', '5', '9', '23', '27', '-4', '29', '8', '2013-10-16 13:37:40', '2013-10-16 13:37:40');
INSERT INTO `tables` VALUES ('8', '8', '7', '22', '8', '4', '10', '34', '34', '0', '28', '8', '2013-10-16 13:40:33', '2013-10-16 13:40:33');
INSERT INTO `tables` VALUES ('9', '9', '12', '22', '7', '6', '9', '25', '31', '-6', '27', '8', '2013-10-16 13:41:40', '2013-10-16 13:41:40');
INSERT INTO `tables` VALUES ('10', '10', '6', '22', '5', '4', '13', '19', '33', '-14', '19', '8', '2013-10-16 13:42:19', '2013-10-16 13:42:19');
INSERT INTO `tables` VALUES ('11', '11', '3', '22', '4', '7', '11', '13', '35', '-22', '19', '8', '2013-10-16 13:42:59', '2013-10-16 13:42:59');
INSERT INTO `tables` VALUES ('12', '12', '5', '22', '1', '7', '14', '11', '28', '-17', '10', '8', '2013-10-16 13:43:30', '2013-10-16 13:43:30');

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team` varchar(200) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of teams
-- ----------------------------
INSERT INTO `teams` VALUES ('1', 'ATM', '/files/picture/team/atmfa_150.png', '2013-10-16 08:14:50');
INSERT INTO `teams` VALUES ('2', 'DARUL TAKZIM', '/files/picture/team/johorfc_150.png', '2013-10-16 08:15:48');
INSERT INTO `teams` VALUES ('3', 'FELDA UNITED', '/files/picture/team/feldaunited_150.png', '2013-10-16 08:16:14');
INSERT INTO `teams` VALUES ('4', 'KELANTAN', '/files/picture/team/kelantanfa_150.png', '2013-10-16 08:16:38');
INSERT INTO `teams` VALUES ('5', 'NEGERI SEMBILAN', '/files/picture/team/negerisembilanfa_150.png', '2013-10-16 08:17:06');
INSERT INTO `teams` VALUES ('6', 'T-TEAM', '/files/picture/team/tteamfc_150.png', '2013-10-16 08:17:29');
INSERT INTO `teams` VALUES ('7', 'PKNS', '/files/picture/team/pknsfc_150.png', '2013-10-16 08:17:54');
INSERT INTO `teams` VALUES ('8', 'Pahang', '/files/picture/team/pahangfa_150.png', '2013-10-16 08:18:11');
INSERT INTO `teams` VALUES ('9', 'Perak', '/files/picture/team/perakfa_150.png', '2013-10-16 08:18:21');
INSERT INTO `teams` VALUES ('10', 'Selangor', '/files/picture/team/selangorfa_150.png', '2013-10-16 08:18:48');
INSERT INTO `teams` VALUES ('11', 'SINGAPORE LIONSXII ', '/files/picture/team/lionxii_150.png', '2013-10-16 08:19:15');
INSERT INTO `teams` VALUES ('12', 'Terengganu', '/files/picture/team/terengganufa_150.png', '2013-10-16 08:19:56');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `nickName` varchar(200) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('8', 'Ahmad Shah', 'dreadlock.mz@gmail.com', 'nick', 'ddffdd57fd0498200c82f64a20bc20d4449171fc', '1', '2013-02-15 06:47:12', '2013-02-15 06:47:12');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `extension` varchar(200) DEFAULT NULL,
  `mimeType` varchar(200) DEFAULT NULL,
  `fileSize` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos
-- ----------------------------

-- ----------------------------
-- Table structure for videos_categories
-- ----------------------------
DROP TABLE IF EXISTS `videos_categories`;
CREATE TABLE `videos_categories` (
  `video_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos_categories
-- ----------------------------
