<div id="page-container" class="row">

	<!-- element -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Advertisment'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						<tr>
							<!-- element -->								
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('picture1'); ?></th>
							<th><?php echo $this->Paginator->sort('picture2'); ?></th>
							<th><?php echo $this->Paginator->sort('picture3'); ?></th>
			
							<th><?php echo $this->Paginator->sort('created'); ?></th>
		
							<th class="actions"><?php echo __('Actions'); ?></th>								
															
						</tr>
					</thead>
					<tbody>
						<?php foreach ($advertisments as $advertisment): ?>
	<tr>
		<td><?php echo h($advertisment['Advertisment']['id']); ?>&nbsp;</td>
		<td><?php if($advertisment['Advertisment']['picture1'] != NULL) { echo "<img src='".$this->webroot . $advertisment['Advertisment']['picture1']."'  height='150' width='100'/>"; }else { echo 'Not Provided';}?>&nbsp;</td>
		<td><?php if($advertisment['Advertisment']['picture2'] != NULL) { echo "<img src='".$this->webroot . $advertisment['Advertisment']['picture2']."'  height='150' width='100'/>"; }else { echo 'Not Provided';}?>&nbsp;</td>
		<td><?php if($advertisment['Advertisment']['picture3'] != NULL) { echo "<img src='".$this->webroot . $advertisment['Advertisment']['picture3']."'  height='150' width='100'/>"; }else { echo 'Not Provided';}?>&nbsp;</td>
		
		<td><?php echo h($advertisment['Advertisment']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $advertisment['Advertisment']['id']), null, __('Are you sure you want to delete # %s?', $advertisment['Advertisment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			
				<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


