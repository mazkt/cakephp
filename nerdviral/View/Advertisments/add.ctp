
<div id="page-container" class="row">

		<!-- element action -->
		<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">

		<div class="profiles form">
		
			<?php echo $this->Form->create('Advertisment', array('type'=>'file','inputDefaults' => array('label' => false), 'role' => 'form')); ?>
		
<fieldset>
		<h2><?php echo __('Add Advertisment'); ?></h2>
		<div class="form-group">
		<fieldset>
		<legend>Ads size banner 1 - height='90' width='728'</legend>
		<?php echo $this->Form->input('picture1',array('label' => 'Banner 1','type'=>'file','class' => 'form-control')); ?>
		</fieldset></div>
		<div class="form-group">
		<fieldset>
		<legend>Ads size banner 2 -  height='60' width='234'</legend>
		<?php echo $this->Form->input('picture2',array('label' => 'Banner 2','type'=>'file','class' => 'form-control')); ?>
		</fieldset></div>
		<div class="form-group">
		<fieldset>
		<legend>Ads size banner 3 - height='300' width='600'</legend>
		<?php echo $this->Form->input('picture3',array('label' => 'Banner 3','type'=>'file','class' => 'form-control')); ?>
		</fieldset></div>
		
		<?php echo $this->Form->input('user_id',array('type'=>'hidden', 'value'=> $this->Session->Read('Auth.User.id'))); ?>
	
	</fieldset>
	
<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->






