<div id="page-container" class="row">

	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">
		
		<div class=" view">

			<h2><?php  echo __('Role'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>	
				<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($role['Role']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($role['Role']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($role['Role']['created']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
