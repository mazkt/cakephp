

<div id="page-container" class="row">

		<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" form">
		
			<?php echo $this->Form->create('Role', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Edit Role'); ?></h2>
					<div class="form-group">
	
		<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
			<div class="form-group">
	<?php echo $this->Form->label('name', 'Name');?>
		<?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
</div><!-- .form-group -->



				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

