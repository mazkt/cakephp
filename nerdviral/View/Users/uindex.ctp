<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Profile'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
								<tr>
			
			<th><?php echo $this->Paginator->sort('fullName'); ?></th>
		
			<th><?php echo $this->Paginator->sort('team_id','Team'); ?></th>
		
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>								
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

<?php foreach ($users as $user): ?>
	<tr>
		
		<td><?php echo h($user['User']['fullName']); ?>&nbsp;</td>
		
	<td>
			<?php echo $this->Html->link($user['Team']['team'], array('controller' => 'teams', 'action' => 'view', $user['Team']['id'])); ?>
		</td>
		

		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'uview', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'editp', $user['User']['id'])); ?>
			
		</td>
	</tr>
<?php endforeach; ?>

							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


