<style type="text/css">


input.file {
	position: relative;
	text-align: right;
	-moz-opacity:0 ;
	filter:alpha(opacity: 0);
	opacity: 0;
	z-index: 2;
}

</style>



<div id="page-container" class="row">

		<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class="profiles form">
		
			<?php echo $this->Form->create('User', array('type' => 'file','inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Edit User'); ?></h2>
					<div class="form-group">
	
		<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
			<div class="form-group">
	<?php echo $this->Form->label('fullName', 'fullName');?>
		<?php echo $this->Form->input('fullName', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('email', 'email');?>
		<?php echo $this->Form->input('email', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('phone', 'phone');?>
		<?php echo $this->Form->input('phone', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
<div class="form-group">
	<?php echo $this->Form->label('picture', 'picture');?>
		<?php echo $this->Form->input('picture', array('type' =>'file','class' => 'form-control', 'id'=>'')); ?>
</div><!-- .form-group -->
<div class="form-group">
	<?php echo $this->Form->label('team_id', 'Team');?>
		<?php echo $this->Form->input('team_id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->




				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

