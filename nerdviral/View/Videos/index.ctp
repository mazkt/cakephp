<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Videos '); ?><?php echo "[".$countVid."]"; ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
						<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			
			<th><?php echo $this->Paginator->sort('user_id','Author'); ?></th>
			
			<th class="actions"><?php echo __('Actions'); ?></th>
			
	</tr>									
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

<?php foreach ($videos as $video): ?>
	<tr>
	
		<td><?php echo h($video['Video']['name']); ?>&nbsp;</td>
		
		<td><?php echo h($video['Video']['date']); ?>&nbsp;</td>
	
		<td>
			<?php echo h($video['User']['fullName']); ?>
		</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $video['Video']['id'])); ?>
			<?php if ($this->Session->read('Auth.User.role_id') == 1)  echo $this->Html->link(__('Edit'), array('action' => 'edit', $video['Video']['id'])); ?>
			<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $video['Video']['id']), null, __('Are you sure you want to delete # %s?', $video['Video']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>

							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

