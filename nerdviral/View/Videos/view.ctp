
<div id="page-container" class="row">

	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">
		
		<div class="profiles view">

			<h2><?php  echo __('Video'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>	
				<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['description']); ?>
			&nbsp;
		</td>
</tr>
<tr>		<td><strong><?php echo __('Date'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['date']); ?>
			&nbsp;
		</td>
</tr>					


<tr>		<td><strong><?php echo __('Video'); ?></strong></td>
		<td >
				<?php
				 
			if($video['Video']['filename'] != NULL) {
				echo "<video  poster='/nerdviral/webroot/img/Speed.jpg' src=".$this->webroot . $video['Video']['filename']."  controls
  preload='none'  class='video-js vjs-default-skin one' data-setup='{}' width ='100%' />";
			}else {
				echo 'Tidak Disediakan';
			}
			?>
			&nbsp;
		</td>
</tr>	
<tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['created']); ?>
			&nbsp;
		</td>
</tr>	




</tbody>
				</table><!-- /.table table-striped table-bordered -->
				<?php echo $this->Comments->display_for($video); ?>
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->

