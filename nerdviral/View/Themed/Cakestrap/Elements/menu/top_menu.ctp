

  
  <h3 class="text-muted">Pappermint Football League</h3>
        <p align="right"><?php 
		
		if ($this->Session->read('Auth.User')) {
				
			echo "<font color=red>Login as ".$this->gravatar->image($this->Session->read('Auth.User.email'), array('size' => 30))."&nbsp"."<b>".$this->Session->read('Auth.User.fullName')."</b>"." "."</font>";
			
			echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));
		}
		else{
			
		
			echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));
			echo __(' || ');
			echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register'));
			}
		?></p>
<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button><!-- /.navbar-toggle -->
		<?php echo $this->Html->Link('Home', '/',array('class' => 'navbar-brand')); ?>
	</div><!-- /.navbar-header -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li><?php echo $this->Html->link('Video', array('controller' => 'videos', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('Headline', array('controller' => 'headlines', 'action' => 'index')); ?></a></li>
            <li><?php echo $this->Html->link('Gossip', array('controller' => 'gossips', 'action' => 'index')); ?></li>
           
            <li><?php echo $this->Html->link('Teams', array('controller' => 'teams', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('League Table', array('controller' => 'tables', 'action' => 'index')); ?></li>
			 <li><?php echo $this->Html->link('All User', array('controller' => 'users', 'action' => 'vindex')); ?></li>
		</ul><!-- /.nav navbar-nav -->
	</div><!-- /.navbar-collapse -->
</nav><!-- /.navbar navbar-default -->