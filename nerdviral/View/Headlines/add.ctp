

<div id="page-container" class="row">

		<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class="profiles form">
		
			<?php echo $this->Form->create('Headline', array('inputDefaults' => array('label' => false), 'role' => 'form','type'=>'file')); ?>
				<fieldset>
					<h2><?php echo __('Add Headline'); ?></h2>
			<div class="form-group">
	<?php echo $this->Form->label('team_id', 'Team');?>
		<?php echo $this->Form->input('team_id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('title', 'Title');?>
		<?php echo $this->Form->input('title', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('description', 'Description');?>
		<?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
<div class="form-group">
	<?php echo $this->Form->label('date', 'Date');?>
		<?php echo $this->Form->input('date', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
<div class="form-group">
	<?php echo $this->Form->label('picture', 'Picture');?>
		<?php echo $this->Form->input('picture', array('class' => 'form-control','type'=> 'file')); ?>
</div><!-- .form-group -->



<div class="form-group">

		<?php echo $this->Form->input('user_id',array('class' => 'form-control','type'=>'hidden','value'=> $this->Session->Read('Auth.User.id'))); ?>
</div><!-- .form-group -->




				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
