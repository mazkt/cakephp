<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
		<h2><?php echo __('Headlines'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
							<tr>
			
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			
			<th><?php echo $this->Paginator->sort('team'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id','Author'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>								
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

							<?php foreach ($headlines as $headline): ?>
	<tr>
		
		<td><?php echo h($headline['Headline']['title']); ?>&nbsp;</td>
		
		<td><?php echo h($headline['Team']['team']); ?>&nbsp;</td>
		<td><?php echo h($headline['Headline']['date']); ?>&nbsp;</td>
		
		<td>
			<?php echo h($headline['User']['fullName']); ?>
		</td>
		<td><?php echo h($headline['Headline']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $headline['Headline']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $headline['Headline']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $headline['Headline']['id']), null, __('Are you sure you want to delete # %s?', $headline['Headline']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>

							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


