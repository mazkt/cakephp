<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */
$headlines = $this->requestAction('headlines/lastheadlines/10');
?>
 

<ul>
<?php foreach ($headlines as $headline): ?>
	<li><i><?php echo h($headline['Headline']['date']); ?></i> : <?php echo h($headline['Headline']['title']); ?>...<?php echo $this->Html->link(__('View'), array('controller' => 'headlines','action' => 'view', $headline['Headline']['id'])); ?></li>
<?php endforeach; ?>
</ul>