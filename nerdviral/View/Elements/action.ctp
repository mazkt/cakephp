<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
			<li class="list-group-item"><?php echo __('Add'); ?></li>	
				<li class="list-group-item"><?php echo $this->Html->link(__('New Headline'), array('controller' => 'headlines','action' => 'add'), array('class' => '')); ?></li>						
				<li class="list-group-item"><?php echo $this->Html->link(__('New Gossip'), array('controller' => 'gossips', 'action' => 'add'), array('class' => '')); ?></li> 
				<li class="list-group-item"><?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add'), array('class' => '')); ?></li> 
				<li class="list-group-item"><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add'), array('class' => '')); ?></li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => '')); ?></li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Advertisment'), array('controller' => 'advertisments', 'action' => 'add'), array('class' => '')); ?></li>
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->