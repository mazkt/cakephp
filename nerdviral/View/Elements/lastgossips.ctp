

<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */
$gossips = $this->requestAction('gossips/lastgossips/5');
?>
 

 
<ul>
<?php foreach ($gossips as $gossip): ?>
	<li><i><?php echo h($gossip['Gossip']['date']); ?></i> : <?php echo h($gossip['Gossip']['title']); ?>...<?php echo $this->Html->link(__('View'), array('controller' => 'gossips','action' => 'view', $gossip['Gossip']['id'])); ?></li>
<?php endforeach; ?>
</ul>