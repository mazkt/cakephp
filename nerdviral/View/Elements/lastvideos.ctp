<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */
$videos = $this->requestAction('videos/lastvideos/5');
?>
 

 
<ul>
<?php foreach ($videos as $video): ?>
	<li><i><?php echo h($video['Video']['date']); ?></i> : <?php echo h($video['Video']['name']); ?>...<?php echo $this->Html->link(__('View'), array('controller' => 'videos','action' => 'view', $video['Video']['id'])); ?></li>
<?php endforeach; ?>
</ul>