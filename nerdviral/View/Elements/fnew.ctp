<?php 
/* First step: get the latest posts, the URL should be like your_controller_name/method_name/params */
$headlines = $this->requestAction('headlines/fnew/1');
?>
 

<ul>
<?php foreach ($headlines as $headline): ?>
	<h2><?php echo h($headline['Headline']['title']); ?></h2> <br/> <p><?php echo h($headline['Headline']['description']); ?></p>
	<?php //echo $this->Html->link(__('View'), array('controller' => 'headlines','action' => 'view', $headline['Headline']['id'])); ?>
<?php endforeach; ?>
</ul>