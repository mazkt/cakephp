

<div id="page-container" class="row">

		<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class="profiles form">
		
			<?php echo $this->Form->create('Table', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Edit League Table'); ?></h2>
					<div class="form-group">
	
		<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->
			<div class="form-group">
	<?php echo $this->Form->label('position', 'position');?>
		<?php echo $this->Form->input('position', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('team_id', 'team');?>
		<?php echo $this->Form->input('team_id', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('played', 'played');?>
		<?php echo $this->Form->input('played', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('draw', 'draw');?>
		<?php echo $this->Form->input('draw', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('lost', 'lost');?>
		<?php echo $this->Form->input('lost', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('goalFor', 'goalFor');?>
		<?php echo $this->Form->input('goalFor', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('goalAgainst', 'goalAgainst');?>
		<?php echo $this->Form->input('goalAgainst', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('goalDifferent', 'goalDifferent');?>
		<?php echo $this->Form->input('goalDifferent', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	<?php echo $this->Form->label('point', 'point');?>
		<?php echo $this->Form->input('point', array('class' => 'form-control')); ?>
</div><!-- .form-group -->

<div class="form-group">
	
		<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'value'=>$this->Session->Read('Auth.User.id'),'type'=>'hidden')); ?>
</div><!-- .form-group -->


				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

