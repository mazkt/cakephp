<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Tables'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
									<tr>
			
			<th><?php echo $this->Paginator->sort('position'); ?></th>
			<th><?php echo $this->Paginator->sort('team_id'); ?></th>
			<th><?php echo $this->Paginator->sort('played','p'); ?></th>
			<th><?php echo $this->Paginator->sort('won','w'); ?></th>
			<th><?php echo $this->Paginator->sort('draw','d'); ?></th>
			<th><?php echo $this->Paginator->sort('lost','l'); ?></th>
			<th><?php echo $this->Paginator->sort('goalFor','gf'); ?></th>
			<th><?php echo $this->Paginator->sort('goalAgainst','ga'); ?></th>
			<th><?php echo $this->Paginator->sort('goalDifferent','gd'); ?></th>
			<th><?php echo $this->Paginator->sort('point','pts'); ?></th>
			
			
			
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>						
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

<?php foreach ($tables as $table): ?>
	<tr>
		
		<td><?php echo h($table['Table']['position']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($table['Team']['team'], array('controller' => 'teams', 'action' => 'view', $table['Team']['id'])); ?>
		</td>
		<td><?php echo h($table['Table']['played']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['won']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['draw']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['lost']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['goalFor']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['goalAgainst']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['goalDifferent']); ?>&nbsp;</td>
		<td><?php echo h($table['Table']['point']); ?>&nbsp;</td>
		
		
		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $table['Table']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $table['Table']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $table['Table']['id']), null, __('Are you sure you want to delete # %s?', $table['Table']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>

							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


