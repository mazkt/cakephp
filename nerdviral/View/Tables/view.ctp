
<div id="page-container" class="row">

	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">
		
		<div class="profiles view">

			<h2><?php  echo __(''); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						
		<tr>		<td><strong><?php echo __('Position'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['position']); ?>
			&nbsp;
		</td>
</tr>
	<tr>		<td><strong><?php echo __('Team'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($table['Team']['team'], array('controller' => 'teams', 'action' => 'view', $table['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Played'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['played']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Won'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['won']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Draw'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['draw']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Lost'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['lost']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Goal For'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['goalFor']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('GoalAgainst'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['goalAgainst']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('GoalDifferent'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['goalDifferent']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Point'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['point']); ?>
			&nbsp;
		</td>
</tr>
			<tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($table['Table']['created']); ?>
			&nbsp;
		</td>
</tr>


		</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->


