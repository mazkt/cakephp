<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html>

<html class="no-js">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo __('Pappermint Football League'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('video-js');
		//echo $this->Html->css('jquery.custom');
		//echo $this->Html->css('cake.generic');
		//echo $this->Html->script('autoload/video.js');
		//echo $this->Html->script('jquery');
		//echo $this->Html->script('jquery.custom');
		//echo $this->Html->script('video');
		//echo $this->Html->script('jquery-1.9.1');
		//echo $this->Html->script('video.min');
		//echo $this->Html->script('bootstrap');

		//echo $this->Html->css('navbar');
		//echo $this->Html->css('bootstrap-responsive');
		//echo $this->Html->css('bootstrap');

		//Groundwork 
		//echo $this->Html->script('modernizr-2.6.2.min');
		//echo $this->Html->script('jquery-1.10.2.min');
		
		//echo $this->Html->css('groundwork');
		//echo $this->Html->css('demo/jquery.snippet');
		//echo $this->Html->script('demo/jquery.snippet.min');
		
		//Groundwork 

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
	




</head>


<body>
	
      <div class="container">
			
		<div class="row">
          
          <div class="one half">
            <p class="small double pad-top no-pad-small-tablet align-right align-left-small-tablet">
            <?php 
		
		if ($this->Session->read('Auth.User')) {
				
			echo "<font color=red>Login as ".$this->gravatar->image($this->Session->read('Auth.User.email'), array('size' => 30))."&nbsp"."<b>".$this->Session->read('Auth.User.fullName')."</b>"." "."</font>";
			
			echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));
		}
		else{
			
		
			echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));
			echo __(' || ');
			echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register'));
			}
		?>
		</p>

        </div>
        </div>

        <nav role="navigation" class="nav gap-top">
          <ul role="menubar">
            <li><i class="icon-home"></i> </li>
            <li role="menu">
              <button>Menu</button>
              <ul>
            <li><?php echo $this->Html->link('Video', array('controller' => 'videos', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('Headline', array('controller' => 'headlines', 'action' => 'index')); ?></a></li>
            <li><?php echo $this->Html->link('Gossip', array('controller' => 'gossips', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('Advertisments', array('controller' => 'advertisments', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('Teams', array('controller' => 'teams', 'action' => 'index')); ?></li>
            <li><?php echo $this->Html->link('League Table', array('controller' => 'tables', 'action' => 'index')); ?></li>
              </ul>
            </li>
            
          </ul>
        </nav>
		</div>
		




		<div class="container">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>





		 <footer class="gap-top bounceInUp animated">
      <div class="box square charcoal">
        <div class="container padded">
          <div class="row">
            <div class="one small-tablet fourth padded">
              
            </div>
            <div class="three small-tablet fourths padded">
              <h5 class="blue"></h5>
              <ul class="unstyled three-column two-column-mobile">
               
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="box square">
        <div class="container padded">
          <div class="row">
            <div class="one half padded">
              &copy; All right Reserved, Nerdviral 2013 
              
            </div>
            
          </div>
        </div>
      </div>
    </footer>
	</div>





</body>

</html>
