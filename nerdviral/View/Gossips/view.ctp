<div id="page-container" class="row">

	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">
		
		<div class="gossips view">

			<h2><?php  echo __('Gossip'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>	
				<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($gossip['Gossip']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Title'); ?></strong></td>
		<td>
			<?php echo h($gossip['Gossip']['title']); ?>
			&nbsp;
		</td>
</tr>
<tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($gossip['Gossip']['description']); ?>
			&nbsp;
		</td>
</tr>

<tr>		<td><strong><?php echo __('Date'); ?></strong></td>
		<td>
			<?php echo h($gossip['Gossip']['date']); ?>
			&nbsp;
		</td>
</tr>					


<tr>		<td><strong><?php echo __('Picture'); ?></strong></td>
		<td>
			<?php if($gossip['Gossip']['picture'] != NULL) { echo "<img src='".$this->webroot . $gossip['Gossip']['picture']."' height='150' width='100'/>"; }else { echo 'Not Provided';}?>&nbsp;
			&nbsp;
		</td>
</tr>					



<tr>		<td><strong><?php echo __('User'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($gossip['User']['fullName'], array('controller' => 'users', 'action' => 'view', $gossip['User']['id'])); ?>
			&nbsp;
		</td>
</tr>					


<tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($gossip['Gossip']['modified']); ?>
			&nbsp;
		</td>
</tr>					









</tbody>
				</table><!-- /.table table-striped table-bordered -->

				  
   
  
<?php echo $this->Comments->display_for($gossip); ?>


			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
