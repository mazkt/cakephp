<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Gossips'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
							<tr>
			
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			
			<th><?php echo $this->Paginator->sort('team'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id', 'Author'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>								
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

							<?php foreach ($gossips as $gossip): ?>
	<tr>
		
		<td><?php echo h($gossip['Gossip']['title']); ?>&nbsp;</td>
		<td>
			<?php echo h($gossip['Team']['team']); ?>
		</td>
		<td><?php echo h($gossip['Gossip']['date']); ?>&nbsp;</td>
		
		<td>
			<?php echo h($gossip['User']['fullName']); ?>
		</td>
		<td><?php echo h($gossip['Gossip']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Read'), array('action' => 'view', $gossip['Gossip']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gossip['Gossip']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gossip['Gossip']['id']), null, __('Are you sure you want to delete # %s?', $gossip['Gossip']['id'])); ?>
		</td>
	</tr>

					<?php endforeach; ?>		
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->




