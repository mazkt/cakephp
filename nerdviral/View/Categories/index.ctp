<div id="page-container" class="row">

	<!-- element action -->
	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h2><?php echo __('Category'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
							<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>								
															
						
					</thead>
					<tbody>
							<!-- element foreach-->

							<?php foreach ($categories as $category): ?>
	<tr>
		<td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $category['Category']['id']), null, __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<!-- element paging-->	
			<?php echo $this->element('paging'); ?><br/>
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


