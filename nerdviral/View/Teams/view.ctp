

<div id="page-container" class="row">

	<?php if ($this->Session->read('Auth.User.role_id') == 1) echo $this->element('action'); ?>
	
	<div id="page-content" class="col-sm-9">
		
		<div class="profiles view">

			<h2><?php  echo __('Team'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>	
				<td><strong><?php echo __('Team'); ?></strong></td>
		<td>
			<?php echo h($team['Team']['team']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Logo'); ?></strong></td>
		<td>
		<?php if($team['Team']['picture'] != NULL) { echo "<img src='".$this->webroot . $team['Team']['picture']."' height='50'/>"; }else { echo 'Not Provided';}?>&nbsp;
			&nbsp;
		</td>
</tr>			</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->

<div id="page-container" class="row">

	<!-- element action -->
	
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h3><?php echo __('Related Gossips'); ?></h3>
			<?php if (!empty($team['Gossip'])): ?>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
							<tr>
		
		<th><?php echo __('Title'); ?></th>
	
		
		<th><?php echo __('Date'); ?></th>
		
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>	
															
						
					</thead>
					<tbody>
							<!-- element foreach-->
<?php foreach ($team['Gossip'] as $gossip): ?>
		<tr>
			
			<td><?php echo $gossip['title']; ?></td>
			
			
			<td><?php echo $gossip['date']; ?></td>
			
			<td class="actions">
				<?php echo $this->Html->link(__('Read'), array('controller' => 'gossips', 'action' => 'view', $gossip['id'])); ?>
				
			</td>
		</tr>
	<?php endforeach; ?>


							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			<?php endif; ?>
			<!-- element paging-->	
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->






<div id="page-container" class="row">

	<!-- element action -->
	
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h3><?php echo __('Related Headlines'); ?></h3>
			<?php if (!empty($team['Headline'])): ?>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
						
							<tr>
		
		<th><?php echo __('Title'); ?></th>
		
		<th><?php echo __('Date'); ?></th>
		
		
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>								
						
					</thead>
					<tbody>
							<!-- element foreach-->
<?php foreach ($team['Headline'] as $headline): ?>
		<tr>
			
			<td><?php echo $headline['title']; ?></td>
			
			<td><?php echo $headline['date']; ?></td>
			
			<td class="actions">
				<?php echo $this->Html->link(__('Read'), array('controller' => 'headlines', 'action' => 'view', $headline['id'])); ?>
				
			</td>
		</tr>
	<?php endforeach; ?>
							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			<?php endif; ?>
			<!-- element paging-->	
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->









<div id="page-container" class="row">

	<!-- element action -->
	
	<div id="page-content" class="col-sm-9">

		<div class=" index">
		
			<h3><?php echo __('League Tables'); ?></h3>
			<?php if (!empty($team['Table'])): ?>
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						
							<!-- element sort -->								
						<tr>
		
		<th><?php echo __('Rank'); ?></th>
		
		
		<th><?php echo __('P'); ?></th>
		<th><?php echo __('W'); ?></th>
		<th><?php echo __('D'); ?></th>
		<th><?php echo __('L'); ?></th>
		<th><?php echo __('GF'); ?></th>
		<th><?php echo __('GA'); ?></th>
		<th><?php echo __('GD'); ?></th>
		<th><?php echo __('PTS'); ?></th>
		
		
	</tr>
															
						
					</thead>
					<tbody>
							<!-- element foreach-->
							<?php foreach ($team['Table'] as $table): ?>
		<tr>
			
			<td><?php echo $table['position']; ?></td>
			
		
			<td><?php echo $table['played']; ?></td>
			<td><?php echo $table['won']; ?></td>
			<td><?php echo $table['draw']; ?></td>
			<td><?php echo $table['lost']; ?></td>
			<td><?php echo $table['goalFor']; ?></td>
			<td><?php echo $table['goalAgainst']; ?></td>
			<td><?php echo $table['goalDifferent']; ?></td>
			<td><?php echo $table['point']; ?></td>
			
			
		</tr>
	<?php endforeach; ?>
							
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			<?php endif; ?>
			<!-- element paging-->	
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->




