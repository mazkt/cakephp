<?php
App::uses('AppController', 'Controller');
/**
 * Videos Controller
 *
 * @property Video $Video
 */
class VideosController extends AppController {
public $actsAs = array('Uploader.Attachment');
public $components = array('Feedback.Comments' => array('on' => array('admin_view', 'view')));




 public function beforeFilter() {
        $this->Auth->allow( array('controller' => 'videos', 'action' => 'index','view','lastvideos','features'));
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->Video->recursive = 0;
		$this->set('videos', $this->paginate());
		$this->set('countVid',$this->Video->find('count'));
	}



/* For instance I want to display the 5 latest posts on my index page. */
public function lastvideos($limit=5)
{
	/* We retrieve only the required fields, and configure the query. */
	$videos = $this->Video->find('all', array('fields'=>array('Video.id', 'Video.name', 'Video.date'),
							   'recursive'=>0,
							   'order'=>array('Video.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $videos;
	}
 
	$this->set('lastvideos', $videos);
}

public function features($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$videos = $this->Video->find('all', array('fields'=>array('Video.filename'),
							   'recursive'=>0,
							   'order'=>array('Video.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $videos;
	}
 
	$this->set('features', $videos);
}






/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->loadModel('User');
		if (!$this->Video->exists($id)) {
			throw new NotFoundException(__('Invalid video'));
		}
		  


		    // save the comment
        if (!empty($this->request->data['Comment'])) {
            $this->request->data['Comment']['class'] = 'Video'; 
            $this->request->data['Comment']['foreign_id'] = $id;
            $this->Video->Comment->create(); 
            if ($this->Video->Comment->save($this->request->data)) {
                $this->Session->setFlash(__('The Comment has been saved.', true));
                $this->redirect(array('action'=>'view',$id));
            }
            $this->Session->setFlash(__('The Comment could not be saved. Please, try again.', true));

         
        }
      
		$options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
		$this->set('video', $this->Video->find('first', $options));
		  
		
		 $video = $this->Video->read(null, $id);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Video->create();
			if ($this->Video->save($this->request->data)) {
				
				$this->Session->setFlash(__('The video has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The video could not be saved. Please, try again.'));
			}
		}
		$users = $this->Video->User->find('list');
		$categories = $this->Video->Category->find('list');
		$this->set(compact('users', 'categories'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Video->exists($id)) {
			throw new NotFoundException(__('Invalid video'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Video->save($this->request->data)) {
				$this->Session->setFlash(__('The video has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The video could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Video.' . $this->Video->primaryKey => $id));
			$this->request->data = $this->Video->find('first', $options);
		}
		$users = $this->Video->User->find('list');
		$categories = $this->Video->Category->find('list');
		$this->set(compact('users', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Video->id = $id;
		if (!$this->Video->exists()) {
			throw new NotFoundException(__('Invalid video'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Video->delete()) {
			$this->Session->setFlash(__('Video deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Video was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
