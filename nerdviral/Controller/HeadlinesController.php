<?php
App::uses('AppController', 'Controller');
/**
 * Headlines Controller
 *
 * @property Headline $Headline
 * @property PaginatorComponent $Paginator
 */
class HeadlinesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $actsAs = array('Uploader.Attachment');
	public $components = array('Paginator','Feedback.Comments' => array('on' => array('admin_view', 'view')));
	public function beforeFilter() {
        $this->Auth->allow( array('controller' => 'headlines', 'action' => 'index','view','lastheadlines','fnew'));
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Headline->recursive = 0;
		$this->set('headlines', $this->Paginator->paginate());
	}


/* For instance I want to display the 5 latest posts on my index page. */
public function lastheadlines($limit=10)
{
	/* We retrieve only the required fields, and configure the query. */
	$headlines = $this->Headline->find('all', array('fields'=>array('Headline.id', 'Headline.title', 'Headline.date'),
							   'recursive'=>0,
							   'order'=>array('Headline.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $headlines;
	}
 
	$this->set('lastheadlines', $headlines);
}

public function fnew($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$headlines = $this->Headline->find('all', array('fields'=>array('Headline.id', 'Headline.title', 'Headline.description'),
							   'recursive'=>0,
							   'order'=>array('Headline.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $headlines;
	}
 
	$this->set('fnew', $headlines);
}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Headline->exists($id)) {
			throw new NotFoundException(__('Invalid headline'));
		}
		$options = array('conditions' => array('Headline.' . $this->Headline->primaryKey => $id));
		$this->set('headline', $this->Headline->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Headline->create();
			if ($this->Headline->save($this->request->data)) {
				$this->Session->setFlash(__('The headline has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The headline could not be saved. Please, try again.'));
			}
		}
		$teams = $this->Headline->Team->find('list');
		$users = $this->Headline->User->find('list');
		$this->set(compact('teams', 'users'));


	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Headline->exists($id)) {
			throw new NotFoundException(__('Invalid headline'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Headline->save($this->request->data)) {
				$this->Session->setFlash(__('The headline has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The headline could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Headline.' . $this->Headline->primaryKey => $id));
			$this->request->data = $this->Headline->find('first', $options);
		}
		$teams = $this->Headline->Team->find('list');
		$users = $this->Headline->User->find('list');
		$this->set(compact('teams', 'users'));

	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Headline->id = $id;
		if (!$this->Headline->exists()) {
			throw new NotFoundException(__('Invalid headline'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Headline->delete()) {
			$this->Session->setFlash(__('The headline has been deleted.'));
		} else {
			$this->Session->setFlash(__('The headline could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
