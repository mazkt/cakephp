<?php
App::uses('AppController', 'Controller');
/**
 * Advertisments Controller
 *
 * @property Advertisment $Advertisment
 * @property PaginatorComponent $Paginator
 */
class AdvertismentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	
	public $actsAs = array('Uploader.Attachment');
	public $components = array('Paginator','Feedback.Comments' => array('on' => array('admin_view', 'view')));
	public function beforeFilter() {
        $this->Auth->allow( array('controller' => 'gossips', 'action' => 'index','view','ads1','ads2','ads3'));
    }

/* For instance I want to display the 5 latest posts on my index page. */
public function ads1($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$advertisments = $this->Advertisment->find('all', array('fields'=>array('Advertisment.picture1'),
							   'recursive'=>0,
							   'order'=>array('Advertisment.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $advertisments;
	}
 
	$this->set('ads1', $advertisments);
}


public function ads2($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$advertisments = $this->Advertisment->find('all', array('fields'=>array('Advertisment.picture2'),
							   'recursive'=>0,
							   'order'=>array('Advertisment.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $advertisments;
	}
 
	$this->set('ads2', $advertisments);
}

public function ads3($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$advertisments = $this->Advertisment->find('all', array('fields'=>array('Advertisment.picture3'),
							   'recursive'=>0,
							   'order'=>array('Advertisment.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $advertisments;
	}
 
	$this->set('ads3', $advertisments);
}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Advertisment->recursive = 0;
		$this->set('advertisments', $this->Paginator->paginate());
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Advertisment->create();
			if ($this->Advertisment->save($this->request->data)) {
				$this->Session->setFlash(__('The advertisment has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The advertisment could not be saved. Please, try again.'));
			}
		}
		$users = $this->Advertisment->User->find('list');
		$this->set(compact('users'));
	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Advertisment->id = $id;
		if (!$this->Advertisment->exists()) {
			throw new NotFoundException(__('Invalid advertisment'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Advertisment->delete()) {
			$this->Session->setFlash(__('The advertisment has been deleted.'));
		} else {
			$this->Session->setFlash(__('The advertisment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
