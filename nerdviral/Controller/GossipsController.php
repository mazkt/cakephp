<?php
App::uses('AppController', 'Controller');
/**
 * Gossips Controller
 *
 * @property Gossip $Gossip
 * @property PaginatorComponent $Paginator
 */
class GossipsController extends AppController {

/**
 * Components
 *
 * @var array
 */	
   
	public $actsAs = array('Uploader.Attachment');
	public $components = array('Paginator','Feedback.Comments' => array('on' => array('admin_view', 'view')));
	public function beforeFilter() {
        $this->Auth->allow( array('controller' => 'gossips', 'action' => 'index','view','lastgossips','fgossip'));
    }



/* For instance I want to display the 5 latest posts on my index page. */
public function lastgossips($limit=5)
{
	/* We retrieve only the required fields, and configure the query. */
	$gossips = $this->Gossip->find('all', array('fields'=>array('Gossip.id', 'Gossip.title', 'Gossip.date'),
							   'recursive'=>0,
							   'order'=>array('Gossip.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $gossips;
	}
 
	$this->set('lastgossips', $gossips);
}

public function fgossip($limit=1)
{
	/* We retrieve only the required fields, and configure the query. */
	$gossips = $this->Gossip->find('all', array('fields'=>array('Gossip.id', 'Gossip.title', 'Gossip.description'),
							   'recursive'=>0,
							   'order'=>array('Gossip.created desc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $gossips;
	}
 
	$this->set('fgossip', $gossips);
}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Gossip->recursive = 0;
		$this->set('gossips', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Gossip->exists($id)) {
			throw new NotFoundException(__('Invalid gossip'));
		}
		$options = array('conditions' => array('Gossip.' . $this->Gossip->primaryKey => $id));
		$this->set('gossip', $this->Gossip->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Gossip->create();
			if ($this->Gossip->save($this->request->data)) {
				$this->Session->setFlash(__('The gossip has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gossip could not be saved. Please, try again.'));
			}
		}
		$teams = $this->Gossip->Team->find('list');
		$users = $this->Gossip->User->find('list');
		$this->set(compact('teams', 'users'));


		
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Gossip->exists($id)) {
			throw new NotFoundException(__('Invalid gossip'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Gossip->save($this->request->data)) {
				$this->Session->setFlash(__('The gossip has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The gossip could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Gossip.' . $this->Gossip->primaryKey => $id));
			$this->request->data = $this->Gossip->find('first', $options);
		}
		$teams = $this->Gossip->Team->find('list');
		$users = $this->Gossip->User->find('list');
		$this->set(compact('teams', 'users'));

	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Gossip->id = $id;
		if (!$this->Gossip->exists()) {
			throw new NotFoundException(__('Invalid gossip'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Gossip->delete()) {
			$this->Session->setFlash(__('The gossip has been deleted.'));
		} else {
			$this->Session->setFlash(__('The gossip could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
