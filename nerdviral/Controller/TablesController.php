<?php
App::uses('AppController', 'Controller');
/**
 * Tables Controller
 *
 * @property Table $Table
 * @property PaginatorComponent $Paginator
 */
class TablesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Table->recursive = 0;
		$this->set('tables', $this->Paginator->paginate());
	}

	public function beforeFilter() {
        $this->Auth->allow( array('controller' => 'tables', 'action' => 'index','view','ltable'));
    }


public function ltable($limit=12)
{  	$this->loadModel('Team');
	$teams = $this->Table->Team->find('list');
	/* We retrieve only the required fields, and configure the query. */
	$tables = $this->Table->find('all', array('fields'=>array('Table.position', 'Team.team','Team.id', 'Table.point'),
							   'recursive'=>0,
							   'order'=>array('Table.position asc'),
							   'limit'=>$limit));
 
	if(isset($this->params['requested']))
	{
		return $tables;
	}
 
	$this->set('ltable', $tables);
}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Table->exists($id)) {
			throw new NotFoundException(__('Invalid table'));
		}
		$options = array('conditions' => array('Table.' . $this->Table->primaryKey => $id));
		$this->set('table', $this->Table->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Table->create();
			if ($this->Table->save($this->request->data)) {
				$this->Session->setFlash(__('The table has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.'));
			}
		}
		$teams = $this->Table->Team->find('list');
		$users = $this->Table->User->find('list');
		$this->set(compact('teams', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Table->exists($id)) {
			throw new NotFoundException(__('Invalid table'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Table->save($this->request->data)) {
				$this->Session->setFlash(__('The table has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The table could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Table.' . $this->Table->primaryKey => $id));
			$this->request->data = $this->Table->find('first', $options);
		}
		$teams = $this->Table->Team->find('list');
		$users = $this->Table->User->find('list');
		$this->set(compact('teams', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Table->id = $id;
		if (!$this->Table->exists()) {
			throw new NotFoundException(__('Invalid table'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Table->delete()) {
			$this->Session->setFlash(__('The table has been deleted.'));
		} else {
			$this->Session->setFlash(__('The table could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
