<?php
App::uses('AppModel', 'Model');
/**
 * VideosVtag Model
 *
 * @property Video $Video
 * @property Vtag $Vtag
 */
class VideosVtag extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Video' => array(
			'className' => 'Video',
			'foreignKey' => 'video_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vtag' => array(
			'className' => 'Vtag',
			'foreignKey' => 'vtag_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
