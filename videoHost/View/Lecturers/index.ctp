
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('New Lecturer'), array('action' => 'add'), array('class' => '')); ?></li>						<li class="list-group-item"><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index'), array('class' => '')); ?></li> 
		<li class="list-group-item"><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => '')); ?></li> 
		<li class="list-group-item"><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index'), array('class' => '')); ?></li> 
		<li class="list-group-item"><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add'), array('class' => '')); ?></li> 
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="lecturers index">
		
			<h2><?php echo __('Lecturers'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						<tr>
															<th><?php echo $this->Paginator->sort('id'); ?></th>
															<th><?php echo $this->Paginator->sort('fullName'); ?></th>
															<th><?php echo $this->Paginator->sort('email'); ?></th>
															<th><?php echo $this->Paginator->sort('picture'); ?></th>
															<th><?php echo $this->Paginator->sort('type'); ?></th>
															<th><?php echo $this->Paginator->sort('user_id'); ?></th>
															<th><?php echo $this->Paginator->sort('created'); ?></th>
															<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($lecturers as $lecturer): ?>
	<tr>
		<td><?php echo h($lecturer['Lecturer']['id']); ?>&nbsp;</td>
		<td><?php echo h($lecturer['Lecturer']['fullName']); ?>&nbsp;</td>
		<td><?php echo h($lecturer['Lecturer']['email']); ?>&nbsp;</td>
		<td><?php echo h($lecturer['Lecturer']['picture']); ?>&nbsp;</td>
		<td><?php echo h($lecturer['Lecturer']['type']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($lecturer['User']['fullName'], array('controller' => 'users', 'action' => 'view', $lecturer['User']['id'])); ?>
		</td>
		<td><?php echo h($lecturer['Lecturer']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $lecturer['Lecturer']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $lecturer['Lecturer']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lecturer['Lecturer']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $lecturer['Lecturer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
