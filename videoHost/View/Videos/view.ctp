
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
						<li class="list-group-item"><?php echo $this->Html->link(__('Edit Video'), array('action' => 'edit', $video['Video']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Video'), array('action' => 'delete', $video['Video']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $video['Video']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Videos'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Video'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Vcategories'), array('controller' => 'vcategories', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Vcategory'), array('controller' => 'vcategories', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Vtags'), array('controller' => 'vtags', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Vtag'), array('controller' => 'vtags', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="videos view">

			<h2><?php  echo __('Video'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Description'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['description']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Date'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['date']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Filename'); ?></strong></td>
		<td>
			<?php 
			if($video['Video']['filename'] != NULL) {
				echo "<video  poster='' src=".$this->webroot . $video['Video']['filename']."  controls
  preload='auto'  class='video-js vjs-default-skin one' data-setup='{}' width ='100%' />";
			}else {
				echo 'Tidak Disediakan';
			}
			?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Extension'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['extension']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('MimeType'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['mimeType']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('FileSize'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['fileSize']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('User'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($video['User']['fullName'], array('controller' => 'users', 'action' => 'view', $video['User']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($video['Video']['created']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			<?php echo $this->Ratings->display_for($video); ?>
			<?php echo $this->Comments->display_for($video); ?>
		</div><!-- /.view -->

					
			<div class="related">

				<h3><?php echo __('Related Vcategories'); ?></h3>
				
				<?php if (!empty($video['Vcategory'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($video['Vcategory'] as $vcategory): ?>
		<tr>
			<td><?php echo $vcategory['id']; ?></td>
			<td><?php echo $vcategory['name']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vcategories', 'action' => 'view', $vcategory['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vcategories', 'action' => 'edit', $vcategory['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vcategories', 'action' => 'delete', $vcategory['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $vcategory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Vcategory'), array('controller' => 'vcategories', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

					
			<div class="related">

				<h3><?php echo __('Related Vtags'); ?></h3>
				
				<?php if (!empty($video['Vtag'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($video['Vtag'] as $vtag): ?>
		<tr>
			<td><?php echo $vtag['id']; ?></td>
			<td><?php echo $vtag['name']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vtags', 'action' => 'view', $vtag['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vtags', 'action' => 'edit', $vtag['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vtags', 'action' => 'delete', $vtag['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $vtag['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Vtag'), array('controller' => 'vtags', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
