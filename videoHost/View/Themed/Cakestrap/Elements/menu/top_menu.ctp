  <p align="right"><?php 
		
		if ($this->Session->read('Auth.User')) {
				
			echo "<font color=red>Login as "."&nbsp"."<b>".$this->Session->read('Auth.User.fullName')."</b>"." "."</font>";
			
			echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));
		}
		else{
			
		
			echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));
			echo __(' || ');
			echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register'));
			}
		?></p>


<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button><!-- /.navbar-toggle -->
		<?php echo $this->Html->Link('VideoHost', '/', array('class' => 'navbar-brand')); ?>
	</div><!-- /.navbar-header -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">Link</a></li>
			<li><a href="#">Link</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
					<li><a href="#">Something else here</a></li>
					<li><a href="#">Separated link</a></li>
					<li><a href="#">One more separated link</a></li>
				</ul>
			</li>
		</ul><!-- /.nav navbar-nav -->
	</div><!-- /.navbar-collapse -->
</nav><!-- /.navbar navbar-default -->