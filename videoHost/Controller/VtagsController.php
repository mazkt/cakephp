<?php
App::uses('AppController', 'Controller');
/**
 * Vtags Controller
 *
 * @property Vtag $Vtag
 * @property PaginatorComponent $Paginator
 */
class VtagsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Vtag->recursive = 0;
		$this->set('vtags', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Vtag->exists($id)) {
			throw new NotFoundException(__('Invalid vtag'));
		}
		$options = array('conditions' => array('Vtag.' . $this->Vtag->primaryKey => $id));
		$this->set('vtag', $this->Vtag->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Vtag->create();
			if ($this->Vtag->save($this->request->data)) {
				$this->Session->setFlash(__('The vtag has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vtag could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$videos = $this->Vtag->Video->find('list');
		$this->set(compact('videos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Vtag->id = $id;
		if (!$this->Vtag->exists($id)) {
			throw new NotFoundException(__('Invalid vtag'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Vtag->save($this->request->data)) {
				$this->Session->setFlash(__('The vtag has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vtag could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Vtag.' . $this->Vtag->primaryKey => $id));
			$this->request->data = $this->Vtag->find('first', $options);
		}
		$videos = $this->Vtag->Video->find('list');
		$this->set(compact('videos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Vtag->id = $id;
		if (!$this->Vtag->exists()) {
			throw new NotFoundException(__('Invalid vtag'));
		}
		if ($this->Vtag->delete()) {
			$this->Session->setFlash(__('Vtag deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vtag was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
