<?php
App::uses('AppController', 'Controller');
/**
 * Lecturers Controller
 *
 * @property Lecturer $Lecturer
 * @property PaginatorComponent $Paginator
 */
class LecturersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow(); // Letting users register themselves
}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Lecturer->recursive = 0;
		$this->set('lecturers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Lecturer->exists($id)) {
			throw new NotFoundException(__('Invalid lecturer'));
		}
		$options = array('conditions' => array('Lecturer.' . $this->Lecturer->primaryKey => $id));
		$this->set('lecturer', $this->Lecturer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Lecturer->create();
			if ($this->Lecturer->save($this->request->data)) {
				$this->Session->setFlash(__('The lecturer has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lecturer could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$users = $this->Lecturer->User->find('list');
		$students = $this->Lecturer->Student->find('list');
		$this->set(compact('users', 'students'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Lecturer->id = $id;
		if (!$this->Lecturer->exists($id)) {
			throw new NotFoundException(__('Invalid lecturer'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Lecturer->save($this->request->data)) {
				$this->Session->setFlash(__('The lecturer has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lecturer could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Lecturer.' . $this->Lecturer->primaryKey => $id));
			$this->request->data = $this->Lecturer->find('first', $options);
		}
		$users = $this->Lecturer->User->find('list');
		$students = $this->Lecturer->Student->find('list');
		$this->set(compact('users', 'students'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Lecturer->id = $id;
		if (!$this->Lecturer->exists()) {
			throw new NotFoundException(__('Invalid lecturer'));
		}
		if ($this->Lecturer->delete()) {
			$this->Session->setFlash(__('Lecturer deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Lecturer was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
