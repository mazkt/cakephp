<?php
App::uses('AppController', 'Controller');
/**
 * Vcategories Controller
 *
 * @property Vcategory $Vcategory
 * @property PaginatorComponent $Paginator
 */
class VcategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Vcategory->recursive = 0;
		$this->set('vcategories', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Vcategory->exists($id)) {
			throw new NotFoundException(__('Invalid vcategory'));
		}
		$options = array('conditions' => array('Vcategory.' . $this->Vcategory->primaryKey => $id));
		$this->set('vcategory', $this->Vcategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Vcategory->create();
			if ($this->Vcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vcategory has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vcategory could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$videos = $this->Vcategory->Video->find('list');
		$this->set(compact('videos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Vcategory->id = $id;
		if (!$this->Vcategory->exists($id)) {
			throw new NotFoundException(__('Invalid vcategory'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Vcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The vcategory has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vcategory could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Vcategory.' . $this->Vcategory->primaryKey => $id));
			$this->request->data = $this->Vcategory->find('first', $options);
		}
		$videos = $this->Vcategory->Video->find('list');
		$this->set(compact('videos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Vcategory->id = $id;
		if (!$this->Vcategory->exists()) {
			throw new NotFoundException(__('Invalid vcategory'));
		}
		if ($this->Vcategory->delete()) {
			$this->Session->setFlash(__('Vcategory deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Vcategory was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}}
