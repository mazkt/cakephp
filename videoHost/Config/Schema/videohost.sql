/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50612
Source Host           : 127.0.0.1:3306
Source Database       : videohost

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2013-10-27 12:02:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES ('1', null, null, 'mazkt', 'mazkt@rake.com', '1', '123', '10', '2013-10-25 20:42:40');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_model` varchar(100) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `author_ip` varchar(20) DEFAULT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_email` varchar(100) NOT NULL,
  `author_website` varchar(200) DEFAULT NULL,
  `content` text NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_comments_foreign_data` (`foreign_id`,`foreign_model`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', 'Video', '1', '1', '::1', 'mazkt rake', 'mazktrake@gmail.com', '', 'adas', '2013-10-27 03:23:01');

-- ----------------------------
-- Table structure for lecturers
-- ----------------------------
DROP TABLE IF EXISTS `lecturers`;
CREATE TABLE `lecturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lecturers
-- ----------------------------

-- ----------------------------
-- Table structure for lecturers_students
-- ----------------------------
DROP TABLE IF EXISTS `lecturers_students`;
CREATE TABLE `lecturers_students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lecturer_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lecturers_students
-- ----------------------------

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_model` varchar(100) NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `author_ip` varchar(20) DEFAULT NULL,
  `rating` float NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_ratings_foreign_data` (`foreign_id`,`foreign_model`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratings
-- ----------------------------
INSERT INTO `ratings` VALUES ('1', 'Video', '3', '::1', '3.5', '2013-10-27 03:31:24');
INSERT INTO `ratings` VALUES ('2', 'Video', '4', '::1', '2.5', '2013-10-27 03:39:04');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin');
INSERT INTO `roles` VALUES ('2', 'student');
INSERT INTO `roles` VALUES ('3', 'lecturer');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `description` text,
  `type` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of students
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'max', '56a6f5cd2dc17a826bba9fe8b08315011e3e04c8', 'mazkt rake', 'mazktrake@gmail.com', '1', '2013-10-25 19:42:39');
INSERT INTO `users` VALUES ('10', 'mazkt', '56a6f5cd2dc17a826bba9fe8b08315011e3e04c8', 'wan ahmad shah', 'asdasd@sda.com', '1', '2013-10-25 20:42:24');

-- ----------------------------
-- Table structure for vcategories
-- ----------------------------
DROP TABLE IF EXISTS `vcategories`;
CREATE TABLE `vcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vcategories
-- ----------------------------
INSERT INTO `vcategories` VALUES ('1', 'video');
INSERT INTO `vcategories` VALUES ('2', 'music');

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `date` date DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `extension` varchar(200) DEFAULT NULL,
  `mimeType` varchar(200) DEFAULT NULL,
  `fileSize` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES ('3', null, null, null, null, null, null, null, null, '2013-10-27 03:31:18');
INSERT INTO `videos` VALUES ('4', '', '', '2013-10-27', '/files/video/Lorde_-_Royals_US_Version.mp4', 'mp4', 'video/mp4', '31256163', '1', '2013-10-27 03:38:52');

-- ----------------------------
-- Table structure for videos_vcategories
-- ----------------------------
DROP TABLE IF EXISTS `videos_vcategories`;
CREATE TABLE `videos_vcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) DEFAULT NULL,
  `vcategory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos_vcategories
-- ----------------------------

-- ----------------------------
-- Table structure for videos_vtags
-- ----------------------------
DROP TABLE IF EXISTS `videos_vtags`;
CREATE TABLE `videos_vtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) DEFAULT NULL,
  `vtag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of videos_vtags
-- ----------------------------

-- ----------------------------
-- Table structure for vtags
-- ----------------------------
DROP TABLE IF EXISTS `vtags`;
CREATE TABLE `vtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vtags
-- ----------------------------
INSERT INTO `vtags` VALUES ('1', 'video');
